import React, { useState, useEffect } from 'react'
import {
  Container,
  Title
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth, db } from '../../config/firebase'

function TransactionsFetch() {
  const [userTransactions, setUserTransactions] = useState([])
  const [isShowContent, setIsShowContent] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        const uid = user.uid
        db.ref(`transactions/${uid}`).once("value", (snapshot) => {
          if (snapshot.val() !== null) {
            const transactions = Object.values(snapshot.val())
            setUserTransactions(transactions)
          }
          setIsShowContent(true)
        })
      }
    })
  }, [])

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='90%'
        padding='30px'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='absolute'
        top='20px'
      >
        {
          !isShowContent
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={70}
              width={70}
            />
            :
            userTransactions.length > 0
              ?
              <React.Fragment>
                <Title
                  color='black'
                  fontSize='26px'
                >
                  Listado de transferencias realizadas
                </Title>
                <table
                  style={{ width: '900px' }}
                >
                  <thead>
                    <tr>
                      <th style={{ padding: '15px' }}>N°</th>
                      <th style={{ padding: '15px' }}>Tarjeta utilizada</th>
                      <th style={{ padding: '15px' }}>Destinatario</th>
                      <th style={{ padding: '15px' }}>Dinero enviado</th>
                      <th style={{ padding: '15px' }}>Destinatario recibe</th>
                      <th style={{ padding: '15px' }}>Estatus</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      userTransactions.map((transfer, i) => {
                        const n = i + 1
                        return (
                          <tr key={i}>
                            <td style={{ textAlign: 'center', padding: '10px' }}>{n}</td>
                            <td style={{ textAlign: 'center', padding: '10px' }}>{transfer.cardNumber}</td>
                            <td style={{ textAlign: 'center', padding: '10px' }}>{transfer.beneficiarieName}</td>
                            <td style={{ textAlign: 'center', padding: '10px' }}>{transfer.sendMoney} {transfer.sendCoin}</td>
                            <td style={{ textAlign: 'center', padding: '10px' }}>{transfer.receiveMoney} {transfer.receiveCoin}</td>
                            <td style={{ textAlign: 'center', padding: '10px' }}>
                              {
                                transfer.approved === 'waiting'
                                  ? 'En espera'
                                  : transfer.approved === true
                                    ? 'Aprobada'
                                    : transfer.approved === false
                                      ? 'Rechazada'
                                      : null
                              }
                            </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </React.Fragment>
              :
              <Title
                color='black'
                fontSize='26px'
              >
                No hay transferencias pendientes
              </Title>
        }
      </Container>
    </Container>
  )
}

export default TransactionsFetch