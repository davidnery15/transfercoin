import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
// Redux
import { Provider } from 'react-redux'
import generateStore from './redux/store'
// Libraries
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Redirector } from 'react-router-redirect'
// Components
import Home from './components/home/HomeView'
import Signup from './components/Signup/SignupView'
import EmailVerification from './components/email-verification/EmailVerificationView'
import ResetPassword from './components/reset-password/ResetPasswordView'
import CalculatorView from './components/calculator/CalculatorView'
import TransactionView from './components/transaction/TransactionView'
import TransactionConfirmView from './components/transaction-confirm/TransactionConfirmView'
import CardsView from './components/cards/CardsView'
import BeneficiariesView from './components/beneficiaries/BeneficiariesView'
import UserTransactionsView from './components/user-transactions/UserTransactionsView'
import ProfileView from './components/profile/ProfileView'
import PanelAdminView from './components/panel-admin/PanelAdminView'

function App() {
  const store = generateStore()
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Redirector />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/registro' component={Signup} />
          <Route path='/verificacion-de-correo' component={EmailVerification} />
          <Route path='/recuperar-contraseña' component={ResetPassword} />
          <Route path='/calculator' component={CalculatorView} />
          <Route path='/transaction' component={TransactionView} />
          <Route path='/transaction-confirm' component={TransactionConfirmView} />
          <Route exact path='/cards' component={CardsView} />
          <Route path='/cards/add-card' component={CardsView} />
          <Route exact path='/beneficiaries' component={BeneficiariesView} />
          <Route path='/beneficiaries/add-beneficiarie' component={BeneficiariesView} />
          <Route path='/user-transactions' component={UserTransactionsView} />
          <Route exact path='/profile' component={ProfileView} />
          <Route exact path='/panel-admin' component={PanelAdminView} />
        </Switch>
      </BrowserRouter>
      <ToastContainer
        position= 'top-right'
        autoClose= {5000}
        hideProgressBar= {false}
        newestOnTop= {false}
        rtl= {false}
        limit={2}
        closeOnClick
        pauseOnVisibilityChange
        draggable
        pauseOnHover
      />
    </Provider>
  );
}

export default App;
