import React from 'react'
import {
  Container,
  Title,
  ButtonNavLink
} from '../styled-components'

import Background from '../../assets/triangle-mosaic.png'

function EmailVerificationView() {
  return (
    <Container
      width='100%'
      height='100vh'
      display='flex'
      alignItems='center'
      justifyContent='center'
      background={Background}
    >
      <Container
        width='600px'
        padding='30px'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        borderRadius='5px'
        background_color='white'
        position='relative'
        display='flex'
        flexDirection='column'
        alignItems='center'
      >
        <Title
          fontWeight='500'
          textAlign='center'
        >
          Revisa tu bandeja de entrada!
          </Title>
        <Title
          fontWeight='500'
          textAlign='center'
        >
          Te hemos enviado un correo de verificación
        </Title>
        <ButtonNavLink
          color='white'
          background_color='black'
          to='/'
        >
          Volver
        </ButtonNavLink>
      </Container>
    </Container>
  )
}

export default EmailVerificationView