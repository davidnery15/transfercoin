import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  Form,
  Input,
  Button,
  ButtonPassword,
  ButtonRecoverPassword,
  ButtonNavLink
} from '../styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { loginUserEmailPass } from '../../redux/userDucks'
import { toast } from 'react-toastify'
import Loader from 'react-loader-spinner'

// Assets
import ShowPassword from '../../assets/showPassword.svg'
import HidePassword from '../../assets/hidePassword.svg'
import Logo from '../../assets/logo.png'

function LoginView() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [passwordVisibility, setPasswordVisibility] = useState(false)

  // Redux
  const dispatch = useDispatch()
  const isFetching = useSelector(store => store.user.isFetching)
  const errorCode = useSelector(store => store.user.errorCode)

  useEffect(() => {
    if (errorCode === 'auth/user-not-found' && !isFetching) {
      toast.error('Usuario no registrado')
    }
    if (errorCode === 'auth/wrong-password' && !isFetching) {
      toast.error('Contraseña incorrecta')
    }
    if (errorCode === 'email-not-verified' && !isFetching) {
      toast.error('Debe verificar su correo para acceder')
    }
  }, [errorCode, isFetching])

  const handleSetEmail = e => setEmail(e.target.value)
  const handleSetPassword = e => setPassword(e.target.value)

  const handlePasswordVisibility = e => {
    e.preventDefault()
    setPasswordVisibility(!passwordVisibility)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (
      email === '' ||
      password === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const regExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
      const isValidateEmail = regExp.test(email)

      if (!isValidateEmail) {
        toast.error('Coloca un correo válido')
      } else {
        dispatch(loginUserEmailPass({ email, password }))
      }
    }
  }

  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      e.preventDefault()
      handleSubmit(e)
    }
  }

  return (
    <Container
      display='flex'
      flexDirection='column'
      alignItems='center'
      justifyContent='center'
    >
      <img
        src={Logo}
        alt='Logo'
      />
      <Form
        width='350px'
        marginTop='30px'
        display='flex'
        flexDirection='column'
        alignItems='center'
        onSubmit={handleSubmit}
      >
        <Title
          fontSize='18px'
          fontWeight='400'
          color='#000'
        >
          Correo
        </Title>
        <Input
          width='97%'
          height='35px'
          type='text'
          textAlign='center'
          onChange={handleSetEmail}
          value={email}
          onKeyPress={handleKeyPress}
        />
        <Title
          fontSize='18px'
          fontWeight='400'
          color='#000'
        >
          Contraseña
        </Title>
        <Container 
          width='100%'
          position='relative'
        >
          <Input
            width='97%'
            height='35px'
            position='absolute'
            textAlign='center'
            type={passwordVisibility ? 'text' : 'password'}
            onChange={handleSetPassword}
            value={password}
            onKeyPress={handleKeyPress}
          />
          <ButtonPassword
            onClick={handlePasswordVisibility}
            background={passwordVisibility ? HidePassword : ShowPassword}
          />
        </Container>
        <ButtonRecoverPassword
          to='/recuperar-contraseña'
        >
          ¿Ha olvidado su contraseña?
        </ButtonRecoverPassword>
        <Button
          width='35%'
          height='40px'
          margin='30px 0 0 0'
          border='1px solid #000'
          color='black'
          background_color='white'
          type='submit'
          disabled={isFetching}
        >
          {
            !isFetching
              ? 'Entrar'
              : <Loader
                type="ThreeDots"
                color="#000"
                height={40}
                width={40}
              />
          }
        </Button>
        <ButtonNavLink
          color='white'
          background_color='black'
          to='/registro'
        >
          Crear cuenta
        </ButtonNavLink>
      </Form>
    </Container>
  )
}

export default LoginView