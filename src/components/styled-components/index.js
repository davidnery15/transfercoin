import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import ArrowBack from '../../assets/arrowBack.svg'
import NumberFormat from 'react-number-format'

export const Container = styled.div`
  padding: ${(props) => (props.padding ? `${props.padding}` : '')};
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  min-height: ${(props) => (props.minHeight ? `${props.minHeight}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  top: ${(props) => (props.top ? `${props.top}` : '')};
  right: ${(props) => (props.right ? `${props.right}` : '')};
  bottom: ${(props) => (props.bottom ? `${props.bottom}` : '')};
  left: ${(props) => (props.left ? `${props.left}` : '')};
  z-index: ${(props) => (props.zIndex ? `${props.zIndex}` : '')};
  border: ${(props) => (props.border ? `${props.border}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  display: ${(props) => (props.display ? `${props.display}` : '')};
  box-shadow: ${(props) => (props.boxShadow ? `${props.boxShadow}` : '')};
  flex-direction: ${(props) => (props.flexDirection ? `${props.flexDirection}` : '')};
  align-items: ${(props) => (props.alignItems ? `${props.alignItems}` : '')};
  justify-content: ${(props) => (props.justifyContent ? `${props.justifyContent}` : '')};
  cursor: ${(props) => (props.cursor ? `${props.cursor}` : '')};
  background-color: ${(props) => (props.background_color ? `${props.background_color}` : '')};
  background-image: ${(props) => (props.background ? `url(${props.background})` : '')};
  background-repeat: ${(props) => (props.backgroundRepeat ? `${props.backgroundRepeat}` : '')};
  grid-template-columns: ${(props) => (props.gridTemplateColumns ? `${props.gridTemplateColumns}` : '')};
`;

export const Title = styled.h2`
  font-weight: ${(props) => (props.fontWeight ? `${props.fontWeight}` : '')};
  font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : '')};
  color: ${(props) => (props.color ? `${props.color}` : '')};
  text-align: ${(props) => (props.textAlign ? `${props.textAlign}` : '')};
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
`;

export const Form = styled.form`
  width: ${(props) => (props.width ? `${props.width}` : '')};
  margin-top: ${(props) => (props.marginTop ? `${props.marginTop}` : '')};
  border: ${(props) => (props.border ? `${props.border}` : '')};
  box-shadow: ${(props) => (props.boxShadow ? `${props.boxShadow}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  padding: ${(props) => (props.padding ? `${props.padding}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  background-color: ${(props) => (props.background_color ? `${props.background_color}` : '')};
  display: ${(props) => (props.display ? `${props.display}` : '')};
  flex-direction: ${(props) => (props.flexDirection ? `${props.flexDirection}` : '')};
  align-items: ${(props) => (props.alignItems ? `${props.alignItems}` : '')};
  justify-content: ${(props) => (props.justifyContent ? `${props.justifyContent}` : '')};
`;

export const Input = styled.input`
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  padding: ${(props) => (props.padding ? `${props.padding}` : '')};
  font-weight: ${(props) => (props.fontWeight ? `${props.fontWeight}` : '')};
  text-align: ${(props) => (props.textAlign ? `${props.textAlign}` : '')};
`;

export const NumberFormatInput = styled(NumberFormat)`
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  padding: ${(props) => (props.padding ? `${props.padding}` : '')};
  font-weight: ${(props) => (props.fontWeight ? `${props.fontWeight}` : '')};
  text-align: ${(props) => (props.textAlign ? `${props.textAlign}` : '')};
`;

export const InputFile = styled.input`
  width: 100%;
  height: 100%;
  opacity: 0;
  cursor: pointer;
`;

export const Select = styled.select`
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
  border: ${(props) => (props.border ? `${props.border}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  color: ${(props) => (props.color ? `${props.color}` : '')};
  outline: ${(props) => (props.outline ? `${props.outline}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  top: ${(props) => (props.top ? `${props.top}` : '')};
`;

export const ButtonRecoverPassword = styled(NavLink)`
  width: 100%;
  cursor: pointer;
  border: none;
  outline: none;
  background: transparent;
  font-size: 16px;
  color: #303962;
  margin-top: 60px;
  text-align: center;
  text-decoration: none;
  &:hover {
    text-decoration: underline;
  }
`;

export const Button = styled.button`
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
  border: ${(props) => (props.border ? `${props.border}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  color: ${(props) => (props.color ? `${props.color}` : '')};
  outline: ${(props) => (props.outline ? `${props.outline}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  top: ${(props) => (props.top ? `${props.top}` : '')};
  right: ${(props) => (props.right ? `${props.right}` : '')};
  bottom: ${(props) => (props.bottom ? `${props.bottom}` : '')};
  left: ${(props) => (props.left ? `${props.left}` : '')};
  background: ${(props) => (props.background ? `${props.background}` : '')};
  background-color: ${(props) => (props.background_color ? `${props.background_color}` : '')};
  display: ${(props) => (props.display ? `${props.display}` : '')};
  flex-direction: ${(props) => (props.flexDirection ? `${props.flexDirection}` : '')};
  align-items: ${(props) => (props.alignItems ? `${props.alignItems}` : '')};
  justify-content: ${(props) => (props.justifyContent ? `${props.justifyContent}` : '')};
  font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : '')};
  font-weight: ${(props) => (props.fontWeight ? `${props.fontWeight}` : '')};
  cursor: pointer;
  &:hover {
    background-color: ${(props) => (props.backgroundHover ? `${props.backgroundHover}` : '')};
  }
`;

export const ButtonNavLink = styled(NavLink)`
  width: 35%;
  height: 40px;
  margin-top: 30px;
  cursor: pointer;
  border: 1px solid #000;
  display: flex;
  justify-content: center;
  align-items: center;
  text-decoration: none;
  color: ${(props) => (props.color ? `${props.color}` : '')};
  background-color: ${(props) => (props.background_color ? `${props.background_color}` : '')};
`;

export const ArrowBackLink = styled(NavLink)`
  width: 25px;
  height: 25px;
  cursor: pointer;
  text-decoration: none;
  background-image: url(${ArrowBack});
  position: absolute;
  top: 15px;
  left: 15px;
`;

export const ButtonPassword = styled.button`
  width: 25px;
  height: 25px;
  cursor: pointer;
  border: none;
  outline: none;
  background: transparent;
  background-image: url(${(props) => (props.background)});
  position: absolute;
  top: 8px;
  right: 12px;
  z-index: 1;
`;

export const NavLinkStyled = styled(NavLink)`
  margin: ${(props) => (props.margin ? `${props.margin}` : '')};
  padding: ${(props) => (props.padding ? `${props.padding}` : '')};
  width: ${(props) => (props.width ? `${props.width}` : '')};
  height: ${(props) => (props.height ? `${props.height}` : '')};
  text-decoration: ${(props) => (props.textDecoration ? `${props.textDecoration}` : '')};
  background: ${(props) => (props.background ? `${props.background}` : '')};
  background-color: ${(props) => (props.background_color ? `${props.background_color}` : '')};
  display: ${(props) => (props.display ? `${props.display}` : '')};
  flex-direction: ${(props) => (props.flexDirection ? `${props.flexDirection}` : '')};
  align-items: ${(props) => (props.alignItems ? `${props.alignItems}` : '')};
  justify-content: ${(props) => (props.justifyContent ? `${props.justifyContent}` : '')};
  color: ${(props) => (props.color ? `${props.color}` : '')};
  font-weight: ${(props) => (props.fontWeight ? `${props.fontWeight}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  transition: ${(props) => (props.transition ? `${props.transition}` : '')};
  position: ${(props) => (props.position ? `${props.position}` : '')};
  border-radius: ${(props) => (props.borderRadius ? `${props.borderRadius}` : '')};
  cursor: pointer;
  &:hover {
    background-color: ${(props) => (props.transitionHover ? `${props.transitionHover}` : '')};
  }
  ${(props) => (props.activeLinkDashboard ? 'background-color: #36558e;' : '')}
  &::before {
    content: '';
    position: absolute;
    left: 0px;
    width: 4px;
    height: 100%;
    ${(props) => (props.activeLinkDashboard ? 'background-color: #0087be;' : '')}
  }
`;
