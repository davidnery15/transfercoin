import React, { useState } from 'react'
import {
  Container,
  ArrowBackLink,
  Title,
  Form,
  Input,
  NumberFormatInput,
  Button
} from '../../styled-components'
import { toast } from 'react-toastify'
import { auth, db } from '../../../config/firebase'

function CardsAdd() {
  const [cardName, setCardName] = useState('')
  const [cardNumber, setCardNumber] = useState('')
  const [cardDate, setCardDate] = useState('')
  const [cardCode, setCardCode] = useState('')

  const handleChangeCardName = e => setCardName(e.target.value)
  const handleChangeCardNumber = e => setCardNumber(e.target.value)
  const handleChangeCardDate = e => setCardDate(e.target.value)
  const handleChangeCardCode = e => setCardCode(e.target.value)

  const handleSubmit = e => {
    e.preventDefault()
    if (
      cardName === '' ||
      cardNumber === '' ||
      cardDate === '' ||
      cardCode === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const nameRegExp = /^[a-z ,.'-]+$/i
      const isValidateName = nameRegExp.test(cardName)

      if (!isValidateName) {
        toast.error('Coloca un nombre válido')
      } else {
        const uid = auth.currentUser.uid
        const data = db.ref(`cards/${uid}`).push()
        data.set({
          cardName,
          cardNumber,
          cardDate,
          cardCode,
          id: data.key
        })
        toast.success('Tarjeta registrada')
        setCardName('')
        setCardNumber('')
        setCardDate('')
        setCardCode('')
      }
    }
  }

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='90%'
        height='60%'
        padding='30px 0 0'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='relative'
        top='20px'
      >
        <ArrowBackLink
          to='/cards'
        />
        <Title
          color='black'
          fontSize='22px'
        >
          Ingrese los datos de su tarjeta para realizar futuras transacciones
        </Title>
        <Form
          width='350px'
          display='flex'
          flexDirection='column'
          onSubmit={handleSubmit}
        >
          <Title
            color='black'
            fontSize='14px'
          >
            Nombre del titular
          </Title>
          <Input
            type='text'
            width='92%'
            height='30px'
            padding='0 10px'
            value={cardName}
            onChange={handleChangeCardName}
          />
          <Title
            color='black'
            fontSize='14px'
          >
            Número de tarjeta
          </Title>
          <NumberFormatInput
            width='92%'
            height='30px'
            padding='0 10px'
            format="####  ####  ####  ####"
            value={cardNumber}
            onChange={handleChangeCardNumber}
          />
          <Container
            width='96%'
            display='flex'
            justifyContent='space-between'
          >
            <Container
              width='45%'
            >
              <Title
                color='black'
                fontSize='14px'
              >
                Fecha de vencimiento
              </Title>
              <NumberFormatInput
                width='92%'
                height='30px'
                padding='0 10px'
                format="## / ##"
                placeholder="mm / aa"
                value={cardDate}
                onChange={handleChangeCardDate}
              />
            </Container>
            <Container
              width='45%'
            >
              <Title
                color='black'
                fontSize='14px'
              >
                Código de seguridad
              </Title>
              <input
                type='password'
                maxLength="3"
                style={{ width: '92%', height: '30px', padding: '0 10px' }}
                value={cardCode}
                onChange={handleChangeCardCode}
              />
            </Container>
          </Container>
          <Container
            width='100%'
            display='flex'
            justifyContent='center'
            margin='30px 0 0'
          >
            <Button
              width='200px'
              height='35px'
              border='none'
              background_color='#36558e'
              color='white'
              fontWeight='400'
              outline='none'
              type='submit'
            >
              Agregar tarjeta
            </Button>
          </Container>
        </Form>
      </Container>
    </Container>
  )
}

export default CardsAdd