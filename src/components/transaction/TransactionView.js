import React, { useState, useEffect } from 'react'
import {
  Container
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth } from '../../config/firebase'
import { push } from 'react-router-redirect'

import Dashboard from '../dashboard/DashboardView'
import NavbarTopView from '../navbar-top/NavbarTopView'
import TransactionCalculator from './transaction-calculator'

function TransactionView() {
  const [pageContent, setPageContent] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        setPageContent(true)
      } else {
        push('/')
      }
    })
  }, [])

  return (
    !pageContent
      ?
      <Container
        width='100%'
        height='100vh'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Loader
          type="ThreeDots"
          color="#000"
          height={100}
          width={100}
        />
      </Container>
      :
      <Container
        width='100%'
      >
        <Container
          width='20%'
          height='100vh'
          position='fixed'
          left='0'
          zIndex='10000'
        >
          <Dashboard />
        </Container>
        <Container
          padding='0 0 0 20%'
        >
          <NavbarTopView />
          <TransactionCalculator />
        </Container>
      </Container>
  )
}

export default TransactionView