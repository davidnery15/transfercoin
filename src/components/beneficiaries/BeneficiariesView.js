import React from 'react'
import {
  Container
} from '../styled-components'

import Dashboard from '../dashboard/DashboardView'
import NavbarTopView from '../navbar-top/NavbarTopView'
import BeneficiariesFetch from './beneficiaries-fetch'
import BeneficiariesAdd from './add-beneficiarie/beneficiaries-add'

function BeneficiariesView() {
  const currentPath = window.location.pathname
  return (
    <Container
      display='grid'
      gridTemplateColumns='20% 80%'
      width='100%'
      height='100vh'
    >
      <Dashboard />
      <Container>
        <NavbarTopView />
        {
          currentPath === '/beneficiaries'
          ? <BeneficiariesFetch />
          : currentPath === '/beneficiaries/add-beneficiarie'
            ? <BeneficiariesAdd />
            : null
          
        }
      </Container>
    </Container>
  )
}

export default BeneficiariesView