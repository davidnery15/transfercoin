import React, { useState, useEffect } from 'react'
import {
  Container,
  Form,
  ArrowBackLink,
  Title,
  Input,
  InputFile,
  Button
} from '../styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { registerUserEmailPass, registerRedirect } from '../../redux/userDucks'
import { push } from 'react-router-redirect'
import { toast } from 'react-toastify'
import Loader from 'react-loader-spinner'

import Logo from '../../assets/logo.png'
import Background from '../../assets/triangle-mosaic.png'
import PictureSignup from '../../assets/pictureSignup.svg'

function SignupView() {
  const [picture, setPicture] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  // Redux
  const dispatch = useDispatch()
  const isRedirect = useSelector(store => store.user.isRedirect)
  const isFetching = useSelector(store => store.user.isFetching)
  const errorCode = useSelector(store => store.user.errorCode)

  const handleSetPicture = e => setPicture(e.target.files[0])
  const handleSetName = e => setName(e.target.value)
  const handleSetEmail = e => setEmail(e.target.value)
  const handleSetPassword = e => setPassword(e.target.value)
  const handleSetConfirmPassword = e => setConfirmPassword(e.target.value)

  useEffect(() => {
    if (errorCode === 'auth/email-already-in-use' && !isFetching) {
      toast.error('El correo ya se encuentra registrado')
    }
  }, [errorCode, isFetching])

  useEffect(() => {
    if (isRedirect) {
      dispatch(registerRedirect())
      push('/verificacion-de-correo')
    }
  }, [isRedirect, dispatch])

  const handleSubmit = e => {
    e.preventDefault()
    if (
      name === '' ||
      email === '' ||
      password === '' ||
      confirmPassword === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const regExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
      const isValidateEmail = regExp.test(email)

      if (!isValidateEmail) {
        toast.error('Coloca un correo válido')
      }
      else if (password.length < 6) {
        toast.error('La contraseña debe tener al menos 6 dígitos')
      }
      else if (password !== confirmPassword) {
        toast.error('Las contraseñas no coinciden')
      }
      else {
        const userInfo = {
          name,
          picture
        }
        dispatch(registerUserEmailPass({ email, password, userInfo }))
      }
    }
  }

  return (
    <Container
      width='100%'
      height='100vh'
      display='flex'
      alignItems='center'
      justifyContent='center'
      background={Background}
    >
      <Form
        width='600px'
        padding='30px'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        position='relative'
        borderRadius='5px'
        background_color='white'
        display='flex'
        flexDirection='column'
        alignItems='center'
        onSubmit={handleSubmit}
      >
        <ArrowBackLink
          to='/'
        />
        <Title
          fontWeight='500'
        >
          Regístrate en
        </Title>
        <img src={Logo} alt='Logo' />
        <Container
          width='100px'
          height='100px'
          margin='50px 0 0 0'
          border='3px dashed #000;'
          cursor='pointer'
          display='flex'
          justifyContent='center'
          alignItems='center'
          position='relative'
        >
          <img
            style={{ position: 'absolute', width: '45px', height: '45px' }}
            src={PictureSignup}
            alt='PictureSignup'
          />
          <InputFile
            type='file'
            accept='image/*'
            onChange={handleSetPicture}
          />
        </Container>
        <Container
          width='95%'
          display='flex'
          justifyContent='space-between'
          margin='50px 0 30px 0'
        >
          <Container
            width='48%'
            display='flex'
            flexDirection='column'
          >
            <label>Nombre completo</label>
            <Input
              height='25px'
              type='text'
              onChange={handleSetName}
              value={name}
            />
          </Container>
          <Container
            width='48%'
            display='flex'
            flexDirection='column'
          >
            <label>Correo</label>
            <Input
              height='25px'
              type='text'
              onChange={handleSetEmail}
              value={email}
            />
          </Container>
        </Container>
        <Container
          width='95%'
          display='flex'
          justifyContent='space-between'
        >
          <Container
            width='48%'
            display='flex'
            flexDirection='column'
          >
            <label>Contraseña</label>
            <Input
              height='25px'
              type='password'
              onChange={handleSetPassword}
              value={password}
            />
          </Container>
          <Container
            width='48%'
            display='flex'
            flexDirection='column'
          >
            <label>Confirmar contraseña</label>
            <Input
              height='25px'
              type='password'
              onChange={handleSetConfirmPassword}
              value={confirmPassword}
            />
          </Container>
        </Container>
        <Button
          width='120px'
          height='40px'
          margin='40px 0 0 0'
          border='1px solid #000'
          background='transparent'
          disabled={isFetching}
          type='submit'
        >
          {
            !isFetching
              ? 'Registrarse'
              : <Loader
                type="ThreeDots"
                color="#000"
                height={40}
                width={40}
              />
          }
        </Button>
      </Form>
    </Container>
  )
}

export default SignupView