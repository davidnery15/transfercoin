import React, { useState, useEffect } from 'react'
import {
  Container,
  Form,
  ArrowBackLink,
  Title,
  Input,
  Button
} from '../styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { resetUserPassword, setResetPassword, setErrorCode } from '../../redux/userDucks'
import { toast } from 'react-toastify'
import Loader from 'react-loader-spinner'

import Background from '../../assets/vintage-wallpaper.png'

function ResetPasswordView() {
  const [email, setEmail] = useState('')
  //Redux
  const dispatch = useDispatch()
  const errorCode = useSelector(store => store.user.errorCode)
  const isFetching = useSelector(store => store.user.isFetching)
  const isSentResetPassword = useSelector(store => store.user.isSentResetPassword)

  const handleSetEmail = e => setEmail(e.target.value)

  useEffect(() => {
    if (errorCode === 'auth/user-not-found' && !isFetching) {
      dispatch(setErrorCode())
      toast.error('Correo no registrado')
    }
  }, [errorCode, isFetching, dispatch])

  useEffect(() => {
    if (isSentResetPassword) {
      dispatch(setResetPassword())
      toast.success('Correo enviado!')
    }
  }, [isSentResetPassword, dispatch])

  const handleSubmit = e => {
    e.preventDefault()
    if (email === '') {
      toast.error('Ingresa un correo')
    } else {
      const regExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
      const isValidateEmail = regExp.test(email)

      if (!isValidateEmail) {
        toast.error('Coloca un correo válido')
      } else {
        dispatch(resetUserPassword({ email }))
      }
    }
  }

  return (
    <Container
      width='100%'
      height='100vh'
      display='flex'
      alignItems='center'
      justifyContent='center'
      background={Background}
    >
      <Form
        width='500px'
        padding='20px'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        position='relative'
        borderRadius='5px'
        background_color='white'
        display='flex'
        flexDirection='column'
        alignItems='center'
        onSubmit={handleSubmit}
      >
        <ArrowBackLink
          to='/'
        />
        <Title
          margin='15px 0 0'
          fontWeight='500'
          fontSize='26px'
          textAlign='center'
        >
          Restablezca su contraseña
        </Title>
        <Title
          fontSize='20px'
          fontWeight='400'
          textAlign='center'
        >
          Ingrese el correo asociado a su cuenta
        </Title>
        <Input
          width='75%'
          height='30px'
          padding='5px 10px'
          fontWeight='400'
          type='text'
          placeholder='Correo'
          onChange={handleSetEmail}
          value={email}
        />
        <Button
          width='80%'
          height='40px'
          margin='20px 0'
          border='1px solid #000'
          background='#2d365f'
          color='white'
          type='submit'
          disabled={isFetching}
        >
          {
            !isFetching
              ? 'Enviar'
              : <Loader
                type="ThreeDots"
                color="#fff"
                height={30}
                width={60}
              />
          }
        </Button>
      </Form>
    </Container>
  )
}

export default ResetPasswordView