import { auth, storage } from '../config/firebase'

// State
const initialState = {
  isFetching: false,
  isRedirect: false,
  isSentResetPassword: false,
  errorCode: null
}

// Types
// REGISTER
const USER_REGISTER_STARTED = 'USER_REGISTER_STARTED'
const USER_REGISTER_DONE = 'USER_REGISTER_DONE'
const USER_REGISTER_ERROR = 'USER_REGISTER_ERROR'
const USER_REGISTER_REDIRECT = 'USER_REGISTER_REDIRECT'
// LOGIN
const USER_LOGIN_STARTED = 'USER_LOGIN_STARTED'
const USER_LOGIN_DONE = 'USER_LOGIN_DONE'
const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR'
// RESET PASSWORD
const USER_RESET_PASSWORD_STARTED = 'USER_RESET_PASSWORD_STARTED'
const USER_RESET_PASSWORD_DONE = 'USER_RESET_PASSWORD_DONE'
const USER_RESET_PASSWORD_ERROR = 'USER_RESET_PASSWORD_ERROR'
const USER_SET_RESET_PASSWORD = 'USER_SET_RESET_PASSWORD'
// SET ERROR CODE
const USER_SET_ERROR_CODE = 'USER_SET_ERROR_CODE'

// Reducer
export default function reducer(state = initialState, action) {
  switch (action.type) {
    //USER REGISTER
    case USER_REGISTER_STARTED:
      return {
        ...state,
        isFetching: true,
        errorCode: null
      }
    case USER_REGISTER_DONE:
      return {
        ...state,
        isFetching: false,
        isRedirect: true
      }
    case USER_REGISTER_ERROR:
      return {
        ...state,
        isFetching: false,
        errorCode: action.payload.errorCode
      }
    case USER_REGISTER_REDIRECT:
      return {
        ...state,
        isRedirect: false
      }
    case USER_LOGIN_STARTED:
      return {
        ...state,
        isFetching: true,
        errorCode: null
      }
    case USER_LOGIN_DONE:
      return {
        ...state,
        isFetching: false
      }
    case USER_LOGIN_ERROR:
      return {
        ...state,
        isFetching: false,
        errorCode: action.payload.errorCode
      }
    case USER_RESET_PASSWORD_STARTED:
      return {
        ...state,
        isFetching: true,
        errorCode: null,
        isSentResetPassword: false
      }
    case USER_RESET_PASSWORD_DONE:
      return {
        ...state,
        isFetching: false,
        isSentResetPassword: true
      }
    case USER_RESET_PASSWORD_ERROR:
      return {
        ...state,
        isFetching: false,
        errorCode: action.payload.errorCode
      }
    case USER_SET_RESET_PASSWORD:
      return {
        ...state,
        isSentResetPassword: false
      }
    case USER_SET_ERROR_CODE:
      return {
        ...state,
        errorCode: null,
      }
    default:
      return state;
  }
}

// Actions

// Register
export const registerUserEmailPass = ({ email, password, userInfo }) => (dispatch) => {
  dispatch({ type: USER_REGISTER_STARTED })
  auth.createUserWithEmailAndPassword(email, password)
    .then(() => {
      auth.currentUser.sendEmailVerification()
        .then(() => {
          dispatch({ type: USER_REGISTER_DONE })
          // const uid = auth.currentUser.uid
          // db.ref(`users/${uid}`).set({
          //   email,
          //   name: userInfo.name
          // })
          if (userInfo.picture !== '') {
            const storageRef = storage.ref(`/userProfile/${auth.currentUser.uid}`)
            const task = storageRef.put(userInfo.picture)
            task.on('state_changed', (snapshot) => {
              //console.log(snapshot)
            }, (error) => {
              //console.log('Error al subir archivo', error)
            }, () => {
              task.snapshot.ref.getDownloadURL().then((downloadURL) => {
                auth.currentUser.updateProfile({
                  displayName: userInfo.name,
                  photoURL: downloadURL
                })
              })
            })
          } else {
            auth.currentUser.updateProfile({
              displayName: userInfo.name
            })
          }
        })
        .catch((error) => {
          //console.log("error al enviar verificacion de correo", error)
        })
    })
    .catch((error) => {
      dispatch({
        type: USER_REGISTER_ERROR,
        payload: {
          errorCode: error.code
        }
      })
    })
}

// Redirect to email verification
export const registerRedirect = () => (dispatch) => {
  dispatch({ type: USER_REGISTER_REDIRECT })
}

//LOGIN
export const loginUserEmailPass = ({ email, password }) => {
  return async (dispatch) => {
    dispatch({ type: USER_LOGIN_STARTED })
    auth.signInWithEmailAndPassword(email.trim(), password.trim())
      .then(() => {
        if (auth.currentUser.emailVerified) {
          dispatch({
            type: USER_LOGIN_DONE
          })
        } else {
          dispatch({
            type: USER_LOGIN_ERROR,
            payload: {
              errorCode: 'email-not-verified'
            }
          })
        }
      })
      .catch((error) => {
        dispatch({
          type: USER_LOGIN_ERROR,
          payload: {
            errorCode: error.code
          }
        })
      })
  }
}

//RESET PASSWORD
export const resetUserPassword = ({ email }) => {
  return async (dispatch) => {
    dispatch({ type: USER_RESET_PASSWORD_STARTED })
    auth.sendPasswordResetEmail(email.trim())
      .then(() => {
        dispatch({
          type: USER_RESET_PASSWORD_DONE
        })
      })
      .catch((error) => {
        dispatch({
          type: USER_RESET_PASSWORD_ERROR,
          payload: {
            errorCode: error.code
          }
        })
      })
  }
}

// Set isSentResetPassword
export const setResetPassword = () => (dispatch) => {
  dispatch({ type: USER_SET_RESET_PASSWORD })
}

// Set errorCode
export const setErrorCode = () => (dispatch) => {
  dispatch({ type: USER_SET_ERROR_CODE })
}