import React from 'react'
import {
  Container,
  Title,
  ButtonNavLink
} from '../styled-components'

function TransactionConfirm() {
  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
    >
      <Container
        width='80%'
        height='200px'
        padding='30px'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        borderRadius='5px'
        background_color='white'
        display='flex'
        flexDirection='column'
        alignItems='center'
        margin='30px 0 0'
      >
        <Title
          fontWeight='500'
          textAlign='center'
        >
          Tu transacción se está procesando.
          </Title>
        <Title
          fontSize='18px'
          textAlign='center'
        >
          En tu bandeja de transacciones puedes verificar el estatus de las transferencias realizadas
        </Title>
        <ButtonNavLink
          color='white'
          background_color='black'
          to='/transaction'
        >
          Realizar otra transferencia
        </ButtonNavLink>
      </Container>
    </Container>
  )
}

export default TransactionConfirm