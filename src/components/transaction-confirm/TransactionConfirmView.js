import React, { useEffect } from 'react'
import {
  Container
} from '../styled-components'
import { auth } from '../../config/firebase'
import { push } from 'react-router-redirect'

import Dashboard from '../dashboard/DashboardView'
import NavbarTopView from '../navbar-top/NavbarTopView'
import TransactionConfirm from './Transaction-confirm-message'

function CalculatorView() {

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (!user) {
        push('/')
      }
    })
  }, [])

  return (
    <Container
      display='grid'
      gridTemplateColumns='20% 80%'
      width='100%'
      height='100vh'
    >
      <Dashboard />
      <Container>
        <NavbarTopView />
        <TransactionConfirm />
      </Container>
    </Container>
  )
}

export default CalculatorView