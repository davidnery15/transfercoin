import React, { useEffect } from 'react'
import { authenticate } from '../../functions/authenticate'
import {
  Container,
  NavLinkStyled,
  Title
} from '../styled-components'

import Logo from '../../assets/logo.png'

const routes = [
  {
    name: 'Página de inicio',
    path: '/calculator',
    iconUrl: require('../../assets/icon-home.svg'),
  },
  {
    name: 'Tarjetas registradas',
    path: '/cards',
    iconUrl: require('../../assets/icon-card.svg'),
  },
  {
    name: 'Destinatarios',
    path: '/beneficiaries',
    iconUrl: require('../../assets/icon-people.svg'),
  },
  {
    name: 'Transferencias',
    path: '/user-transactions',
    iconUrl: require('../../assets/icon-transfer.svg'),
  }
];

function DashboardView() {

  useEffect(() => {
    authenticate()
  }, [])

  return (
    <Container
      height='100vh'
      background_color='#303962'
      position='relative'
    >
      <Container
        width='100%'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='absolute'
        top='30px'
      >
        <NavLinkStyled
          to='/calculator'
          width='170px'
          height='45px'
        >
          <img
            src={Logo}
            alt='Logo'
            style={{ width: '100%', height: '100%' }}
          />
        </NavLinkStyled>
        <NavLinkStyled
          to='/transaction'
          width='70%'
          height='35px'
          display='flex'
          alignItems='center'
          justifyContent='center'
          background_color='#2ed06e'
          color='#fff'
          textDecoration='none'
          margin='20px 0'
          borderRadius='2px'
          transition='all .15s ease-in-out'
          transitionHover='#28b862'
          fontWeight='500'
        >
          Enviar dinero
        </NavLinkStyled>
        {routes.map((route, i) => {
          const currentPath = window.location.pathname

          return (
            <NavLinkStyled
              key={i}
              to={route.path}
              width='100%'
              height='40px'
              background='transparent'
              display='flex'
              alignItems='center'
              textDecoration='none'
              position='relative'
              transitionHover='#36558e'
              activeLinkDashboard={route.path === currentPath ? true : false}
            >
              <img
                src={route.iconUrl}
                alt='home'
                style={{ width: '20px', height: '20px', margin: '0 30px' }}
              />
              <Title
                fontSize='14px'
                color='white'
              >
                {route.name}
              </Title>
            </NavLinkStyled>
          )
        })}
      </Container>
    </Container>
  )
}

export default DashboardView