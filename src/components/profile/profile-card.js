import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  Form,
  Input,
  Button
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth, storage } from '../../config/firebase'
import { toast } from 'react-toastify'

import IconProfile from '../../assets/iconProfile.svg'

function ProfileCard() {
  const [user, setUser] = useState('')
  const [picture, setPicture] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [firebaseEmail, setFirebaseEmail] = useState('')

  const [isShowContent, setIsShowContent] = useState(false)

  const handleChangePicture = e => setPicture(e.target.files[0])
  const handleChangeName = e => setName(e.target.value)
  const handleChangeEmail = e => setEmail(e.target.value)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user)
        setName(user.displayName)
        setEmail(user.email)
        setFirebaseEmail(user.email)
        setIsShowContent(true)
      }
    })
  }, [])

  const handleSubmit = e => {
    e.preventDefault()
    if (
      name === '' ||
      email === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const regExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
      const isValidateEmail = regExp.test(email)

      if (!isValidateEmail) {
        toast.error('Coloca un correo válido')
      } else {
        if (picture !== '') {
          const storageRef = storage.ref(`/userProfile/${auth.currentUser.uid}`)
          const task = storageRef.put(picture)
          task.on('state_changed', (snapshot) => {
            //console.log(snapshot)
          }, (error) => {
            //console.log('Error al subir archivo', error)
          }, () => {
            task.snapshot.ref.getDownloadURL().then((downloadURL) => {
              auth.currentUser.updateProfile({
                displayName: name,
                photoURL: downloadURL
              })
            })
          })
        } else {
          auth.currentUser.updateProfile({
            displayName: name
          })
        }
        if (email !== firebaseEmail) {
          auth.currentUser.updateEmail(email)
            .then(() => {
              auth.currentUser.sendEmailVerification()
            })
            .catch(error => {
              console.log(error)
            })
        }
        toast.success('Datos actualizados correctamente')
        setTimeout(() => window.location.reload(), 5000);
      }
    }
  }

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='90%'
        padding='0 30px'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='absolute'
        top='20px'
      >
        {
          !isShowContent
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={70}
              width={70}
            />
            :
            <React.Fragment>
              <Title
                color='black'
                fontSize='26px'
                margin='50px 0 25px'
              >
                Actualiza los datos de tu perfil
              </Title>
              <Form
                onSubmit={handleSubmit}
                width='100%'
                display='flex'
                flexDirection='column'
                alignItems='center'
                justifyContent='center'
              >
                <Container
                  display='flex'
                >
                  <Container
                    display='flex'
                    flexDirection='column'
                    alignItems='center'
                    justifyContent='center'
                    margin='0 100px 0 0'
                  >
                    <img
                      src={user.photoURL === null ? IconProfile : user.photoURL}
                      alt='Icon-profile'
                      style={user.photoURL === null
                        ?
                        {
                          width: '136px',
                          height: '136px',
                          padding: '30px',
                          border: '2px solid rgb(0 140 197)',
                          borderRadius: '100%',
                          background: 'rgb(226 226 226)'
                        }
                        :
                        {
                          width: '196px',
                          height: '196px',
                          border: '2px solid rgb(0 140 197)',
                          borderRadius: '100%'
                        }
                      }
                    />
                    <Input
                      type='file'
                      accept='image/*'
                      width='155px'
                      margin='20px 0 0'
                      onChange={handleChangePicture}
                    />
                  </Container>
                  <Container
                    width='350px'
                  >
                    <Container
                      width='100%'
                      height='80px'
                    >
                      <Title
                        fontSize='18px'
                        fontWeight='400'
                      >
                        Nombre completo
                      </Title>
                      <Input
                        type='text'
                        width='90%'
                        height='35px'
                        padding='0 15px'
                        value={name}
                        onChange={handleChangeName}
                      />
                    </Container>
                    <Container
                      width='100%'
                      height='80px'
                    >
                      <Title
                        fontSize='18px'
                        fontWeight='400'
                      >
                        Correo
                      </Title>
                      <Input
                        type='text'
                        width='90%'
                        height='35px'
                        padding='0 15px'
                        value={email}
                        onChange={handleChangeEmail}
                      />
                    </Container>
                  </Container>
                </Container>
                <Button
                  type='submit'
                  width='250px'
                  height='35px'
                  display='flex'
                  alignItems='center'
                  justifyContent='center'
                  background_color='#303962'
                  color='#fff'
                  margin='40px 0'
                  border='none'
                  borderRadius='2px'
                >
                  Guardar cambios
                </Button>
              </Form>
            </React.Fragment>
        }
      </Container>
    </Container>
  )
}

export default ProfileCard