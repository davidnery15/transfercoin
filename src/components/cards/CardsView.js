import React from 'react'
import {
  Container
} from '../styled-components'

import Dashboard from '../dashboard/DashboardView'
import NavbarTopView from '../navbar-top/NavbarTopView'
import CardsFetch from './cards-fetch'
import CardsAdd from './add-card/cards-add'

function CardsView() {
  const currentPath = window.location.pathname
  return (
    <Container
      display='grid'
      gridTemplateColumns='20% 80%'
      width='100%'
      height='100vh'
    >
      <Dashboard />
      <Container>
        <NavbarTopView />
        {
          currentPath === '/cards'
          ? <CardsFetch />
          : currentPath === '/cards/add-card'
            ? <CardsAdd />
            : null
          
        }
      </Container>
    </Container>
  )
}

export default CardsView