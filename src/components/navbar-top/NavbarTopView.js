import React, { useState, useEffect } from 'react'
import { auth, storage } from '../../config/firebase'
import { push } from 'react-router-redirect'
import {
  Container,
  Title,
  NavLinkStyled,
  Button,
} from '../styled-components'
import Loader from 'react-loader-spinner'

import IconProfile from '../../assets/iconProfile.svg'
import IconUpdateProfile from '../../assets/icon-updateProfile.svg'
import IconLogout from '../../assets/icon-logout.svg'

function NavbarTopView() {
  const [path, setPath] = useState('')
  const [user, setUser] = useState('')
  const [menuToggle, setMenuToggle] = useState(false)

  const handleMenuToggle = e => {
    e.preventDefault()
    setMenuToggle(!menuToggle)
  }

  const handleLogout = (e) => {
    e.preventDefault()
    auth.signOut()
    push('/')
  }

  document.addEventListener('click', function (e) {
    const target = e.target.id
    if (target !== 'menuBtn') {
      setMenuToggle(false)
    }
  })

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user === null) {
        push('/')
      } else {
        setUser(user)
      }
    })
    const currentPath = window.location.pathname
    switch (currentPath) {
      case '/calculator':
        return setPath('Página de inicio')
      case '/transaction':
        return setPath('Transferencia')
      case '/transaction-confirm':
        return setPath('Transferencia')
      case '/cards':
        return setPath('Tarjetas registradas')
      case '/cards/add-card':
        return setPath('Agregar tarjeta')
      case '/beneficiaries':
        return setPath('Destinatarios')
      case '/user-transactions':
        return setPath('Transferencias pendientes')
      case '/profile':
        return setPath('Perfil de usuario')
      default:
        return setPath('');
    }
  }, [])

  return (
    <Container
      height='10vh'
      background_color='white'
      padding='0 50px'
      display='flex'
      alignItems='center'
      justifyContent='space-between'
    >
      <Title
        color='black'
        fontSize='18px'
        margin='0'
      >
        {path}
      </Title>
      {
        user === ''
          ?
          <Loader
            type="ThreeDots"
            color="#000"
            height={40}
            width={40}
          />
          :
          <Button
            display='flex'
            alignItems='center'
            background='transparent'
            border='none'
            outline='none'
            position='relative'
            onClick={handleMenuToggle}
            id='menuBtn'
          >
            <Container
              width='40px'
              height='40px'
              background_color='#f2f2f2'
              borderRadius='100px'
              display='flex'
              alignItems='center'
              justifyContent='center'
              id='menuBtn'
            >
              <img
                id='menuBtn'
                src={user.photoURL === null ? IconProfile : user.photoURL}
                alt='Icon-profile'
                style={user.photoURL === null
                  ? { width: '28px', height: '28px' }
                  : { width: '100%', height: '100%', borderRadius: '100px' }
                }
              />
            </Container>
            <Title
              color='black'
              fontSize='16px'
              fontWeight='400'
              margin='0 15px'
              id='menuBtn'
            >
              {user.displayName}
            </Title>
            {
              menuToggle
                ?
                <Container
                  width='210px'
                  padding='10px'
                  background_color='#fff'
                  boxShadow='0 0 15px 0 rgba(0,0,0,.25)'
                  position='absolute'
                  top='70px'
                  right='0px'
                  zIndex='1000'
                >
                  <NavLinkStyled
                    to='/profile'
                    width='100%'
                    height='40px'
                    background='transparent'
                    outline='none'
                    display='flex'
                    alignItems='center'
                    textDecoration='none'
                    transitionHover='#f2f2f2'
                  >
                    <img
                      src={IconUpdateProfile}
                      alt='icon-updateProfile'
                      style={{ marginRight: '11px', marginLeft: '7px' }}
                    />
                    <Title
                      fontSize='16px'
                      fontWeight='400'
                      color='black'
                    >
                      Perfil
                    </Title>
                  </NavLinkStyled>
                  <Button
                    width='100%'
                    height='40px'
                    background='transparent'
                    border='none'
                    outline='none'
                    display='flex'
                    alignItems='center'
                    backgroundHover='#f2f2f2'
                    onClick={handleLogout}
                  >
                    <img
                      src={IconLogout}
                      alt='icon-logout'
                      style={{ marginRight: '15px' }}
                    />
                    <Title
                      fontSize='16px'
                      fontWeight='400'
                      color='black'
                    >
                      Finalizar sesión
                    </Title>
                  </Button>
                </Container>
                : null
            }
          </Button>
      }
    </Container>
  )
}

export default NavbarTopView