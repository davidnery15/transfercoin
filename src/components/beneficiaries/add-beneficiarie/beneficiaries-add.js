import React, { useState } from 'react'
import {
  Container,
  ArrowBackLink,
  Title,
  Form,
  Input,
  Button
} from '../../styled-components'
import { toast } from 'react-toastify'
import { auth, db } from '../../../config/firebase'

function BeneficiariesAdd() {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [iban, setIban] = useState('')

  const handleChangeName = e => setName(e.target.value)
  const handleChangeEmail = e => setEmail(e.target.value)
  const handleChangeIban = e => setIban(e.target.value)

  const handleSubmit = e => {
    e.preventDefault()
    if (
      name === '' ||
      email === '' ||
      iban === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const nameRegExp = /^[a-z ,.'-]+$/i
      const isValidateName = nameRegExp.test(name)

      if (!isValidateName) {
        toast.error('Coloca un nombre válido')
      } else {
        const emailRegExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
        const isValidateEmail = emailRegExp.test(email)

        if (!isValidateEmail) {
          toast.error('Coloca un correo válido')
        } else {
          const uid = auth.currentUser.uid
          const data = db.ref(`beneficiaries/${uid}`).push()
          data.set({
            name,
            email,
            iban,
            id: data.key
          })
          toast.success('Destinatario registrado')
          setName('')
          setEmail('')
          setIban('')
        }
      }
    }
  }

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='90%'
        height='60%'
        padding='30px 0 0'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='relative'
        top='20px'
      >
        <ArrowBackLink
          to='/beneficiaries'
        />
        <Title
          color='black'
          fontSize='22px'
        >
          Ingrese los datos del destinatario
        </Title>
        <Form
          width='350px'
          display='flex'
          flexDirection='column'
          onSubmit={handleSubmit}
        >
          <Title
            color='black'
            fontSize='14px'
          >
            Nombre completo
          </Title>
          <Input
            type='text'
            width='92%'
            height='30px'
            padding='0 10px'
            value={name}
            onChange={handleChangeName}
          />
          <Title
            color='black'
            fontSize='14px'
          >
            Correo
          </Title>
          <Input
            type='text'
            width='92%'
            height='30px'
            padding='0 10px'
            value={email}
            onChange={handleChangeEmail}
          />
          <Title
            color='black'
            fontSize='14px'
          >
            N° de IBAN
          </Title>
          <Input
            type='text'
            width='92%'
            height='30px'
            padding='0 10px'
            value={iban}
            onChange={handleChangeIban}
          />
          <Container
            width='100%'
            display='flex'
            justifyContent='center'
            margin='30px 0 0'
          >
            <Button
              width='200px'
              height='35px'
              border='none'
              background_color='#36558e'
              color='white'
              fontWeight='400'
              outline='none'
              type='submit'
            >
              Agregar destinatario
            </Button>
          </Container>
        </Form>
      </Container>
    </Container>
  )
}

export default BeneficiariesAdd