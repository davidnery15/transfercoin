import React, { useState, useEffect } from 'react'
import {
  Container
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth } from '../../config/firebase'
import { push } from 'react-router-redirect'

import Dashboard from '../dashboard/DashboardView'
import NavbarTopView from '../navbar-top/NavbarTopView'
import CalculatorCoins from './calculator-coins'

function CalculatorView() {
  const [pageContent, setPageContent] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        setPageContent(true)
      } else {
        push('/')
      }
    })
  }, [])

  return (
    !pageContent
      ?
      <Container
        width='100%'
        height='100vh'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Loader
          type="ThreeDots"
          color="#000"
          height={100}
          width={100}
        />
      </Container>
      :
      <Container
        display='grid'
        gridTemplateColumns='20% 80%'
        width='100%'
        height='100vh'
      >
        <Dashboard />
        <Container>
          <NavbarTopView />
          <CalculatorCoins />
        </Container>
      </Container>
  )
}

export default CalculatorView