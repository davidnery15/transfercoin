import React, { useState, useEffect } from 'react'
import {
  Container,
  Button,
  Title
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth, db } from '../../config/firebase'
import { push } from 'react-router-redirect'

import Logo from '../../assets/logo.png'

function PanelAdminView() {
  const [transactionsUid, setTransactionsUid] = useState([])

  const [pageContent, setPageContent] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user && user.email === 'gestortransfercoin@gmail.com') {
        setPageContent(true)
      } else {
        push('/')
      }
    })
    db.ref(`transactions`).once("value", (snapshot) => {
      if (snapshot.val() !== null) {
        const transactions = Object.values(snapshot.val())
        setTransactionsUid(transactions)
      }
    })
  }, [])

  const handleLogout = e => {
    e.preventDefault()
    auth.signOut()
    push('/')
  }

  const handleApproved = (e, uid, id) => {
    e.preventDefault()
    const data = db.ref(`transactions/${uid}/${id}`)
    data.update({
      approved: true
    })
    window.location.reload()
  }

  const handleDenied = (e, uid, id) => {
    e.preventDefault()
    const data = db.ref(`transactions/${uid}/${id}`)
    data.update({
      approved: false
    })
    window.location.reload()
  }

  return (
    !pageContent
      ?
      <Container
        width='100%'
        height='100vh'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Loader
          type="ThreeDots"
          color="#000"
          height={100}
          width={100}
        />
      </Container>
      :
      <React.Fragment>
        <Container
          width='100%'
          height='10vh'
          background_color='#303962'
          display='flex'
          alignItems='center'
          justifyContent='space-between'
        >
          <img
            src={Logo}
            alt='Logo'
            style={{ width: '170px', height: '45px', marginLeft: '200px' }}
          />
          <Button
            width='120px'
            height='35px'
            border='none'
            borderRadius='2px'
            margin='0 200px 0 0'
            background='#008bc4'
            color='white'
            outline='none'
            onClick={handleLogout}
          >
            Cerrar sesión
          </Button>
        </Container>
        <Container
          width='100%'
          height='90vh'
          boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
          background_color='#f2f2f2'
          display='flex'
          justifyContent='center'
        >
          <Container
            width='90%'
            height='80%'
            margin='20px 0 0 0'
            padding='15px'
            background_color='#fff'
            boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
            display='flex'
            flexDirection='column'
            alignItems='center'
          >
            <Title
              color='black'
              fontSize='26px'
            >
              Listado de transferencias
            </Title>
            {
              transactionsUid.length > 0
                ?
                <table
                  style={{ width: '90%' }}
                >
                  <thead>
                    <tr>
                      <th style={{ padding: '15px' }}>Numero tarjeta</th>
                      <th style={{ padding: '15px' }}>Destinatario</th>
                      <th style={{ padding: '15px' }}>Correo</th>
                      <th style={{ padding: '15px' }}>IBAN</th>
                      <th style={{ padding: '15px' }}>Dinero enviado</th>
                      <th style={{ padding: '15px' }}>Dinero al destinatario</th>
                      <th style={{ padding: '15px' }}>Aprobar</th>
                      <th style={{ padding: '15px' }}>Rechazar</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      transactionsUid.map((transferId) => {
                        const transactionDetail = Object.values(transferId)
                        return (
                          transactionDetail.map((transfer, i) => {
                            return (
                              <tr key={i}>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.cardNumber}</td>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.beneficiarieName}</td>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.beneficiarieEmail}</td>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.beneficiarieIban}</td>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.sendMoney} {transfer.sendCoin}</td>
                                <td style={{ textAlign: 'center', padding: '10px 0' }}>{transfer.receiveMoney} {transfer.receiveCoin}</td>
                                {
                                  transfer.approved === 'waiting'
                                    ?
                                    <React.Fragment>
                                      <td style={{ textAlign: 'center', padding: '10px 0' }}>
                                        <Button
                                          onClick={(e) => handleApproved(e, transfer.uid, transfer.id)}
                                        >
                                          Aprobar
                                        </Button>
                                      </td>
                                      <td style={{ textAlign: 'center', padding: '10px 0' }}>
                                        <Button
                                          onClick={(e) => handleDenied(e, transfer.uid, transfer.id)}
                                        >
                                          Rechazar
                                        </Button>
                                      </td>
                                    </React.Fragment>
                                    :
                                    transfer.approved === true
                                      ?
                                      <td style={{ textAlign: 'center', padding: '10px 0' }}>Aprobada</td>
                                      :
                                      transfer.approved === false
                                        ?
                                        <td style={{ textAlign: 'center', padding: '10px 0' }}>Rechazada</td>
                                        : null
                                }
                              </tr>
                            )
                          })
                        )
                      })
                    }
                  </tbody>
                </table>
                :
                <Title
                  color='black'
                  fontSize='26px'
                >
                  No hay transferencias pendientes
                </Title>
            }
          </Container>
        </Container>
      </React.Fragment>
  )
}

export default PanelAdminView