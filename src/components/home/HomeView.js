import React, { useState, useEffect } from 'react'
import {
  Container,
  Title
} from '../styled-components'
import { auth } from '../../config/firebase'
import { push } from 'react-router-redirect'
import Loader from 'react-loader-spinner'

// Components
import LoginView from '../login/LoginView'
// Backgrounds
import BackgroundLeft from '../../assets/vintage-wallpaper.png'
import BackgroundRight from '../../assets/triangle-mosaic.png'

function HomeView() {
  const [pageContent, setPageContent] = useState(false)

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user && user.emailVerified && user.email === 'gestortransfercoin@gmail.com') {
        push('/panel-admin')
      } else if (user && user.emailVerified && user.email) {
        push('/calculator')
      } else {
        setPageContent(true)
      }
    })
  }, [])

  return (
    !pageContent
      ?
      <Container
        width='100%'
        height='100vh'
        display='flex'
        alignItems='center'
        justifyContent='center'
      >
        <Loader
          type="ThreeDots"
          color="#000"
          height={100}
          width={100}
        />
      </Container>
      :
      <Container
        width='100%'
        height='100vh'
        display='flex'
      >
        <Container
          width='50%'
          height='100%'
          display='flex'
          alignItems='center'
          justifyContent='center'
          padding='0 90px'
          background={BackgroundLeft}
        >
          <Title
            fontSize='42px'
            fontWeight='500'
            color='#fff'
            textAlign='center'
          >
            La forma más económica y eficaz de intercambiar tus divisas
      </Title>
        </Container>
        <Container
          width='50%'
          height='100%'
          display='flex'
          alignItems='center'
          justifyContent='center'
          background={BackgroundRight}
        >
          <LoginView />
        </Container>
      </Container>
  )
}

export default HomeView