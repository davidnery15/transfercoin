import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  NavLinkStyled
} from '../styled-components'
import Loader from 'react-loader-spinner'

import ImgPetro from '../../assets/petro.png'
import ImgDolar from '../../assets/dolar.png'
import ImgEuro from '../../assets/euro.png'

function CalculatorCoins() {
  const [petro, setPetro] = useState(0)
  const [dolar, setDolar] = useState(0)
  const [euro, setEuro] = useState(0)
  const [estaticDolar, setEstaticDolar] = useState(0)
  const [estaticEuro, setEstaticEuro] = useState(0)
  const [isShowContent, setIsShowContent] = useState(false)

  const handleChangePetro = e => {
    setPetro(e.target.value)
    // Set Dolar
    const dolarValue = (estaticDolar * e.target.value).toFixed(2)
    setDolar(dolarValue)
    // Set Euro
    const euroValue = (estaticEuro * e.target.value).toFixed(2)
    setEuro(euroValue)
  }

  const handleChangeDolar = e => {
    setDolar(e.target.value)
    // Set Petro
    const petroValue = (e.target.value / estaticDolar).toFixed(8)
    setPetro(petroValue)
    // Set Euro
    const euroValue = (e.target.value - (e.target.value * 0.12)).toFixed(2)
    setEuro(euroValue)
  }

  const handleChangeEuro = e => {
    setEuro(e.target.value)
    // Set Petro
    const petroValue = (e.target.value / estaticEuro).toFixed(8)
    setPetro(petroValue)
    // Set Dolar
    const dolarValue = (e.target.value * 1.13).toFixed(2)
    setDolar(dolarValue)
  }

  useEffect(() => {
    fetch('https://s3.amazonaws.com/dolartoday/data.json')
      .then(response => {
        return response.json()
      })
      .then(object => {
        // Set Petro
        const petroValue = (1.00).toFixed(2)
        setPetro(petroValue)
        // Set Dolar
        const stringDolar = object.MISC.petroleo
        const formatedStringDolar = stringDolar.replace(',', '.')
        const dolarValue = parseFloat(formatedStringDolar)
        setDolar(dolarValue)
        setEstaticDolar(dolarValue)
        // Set Euro
        const euroValue = (dolarValue - (dolarValue * 0.09)).toFixed(2)
        setEuro(euroValue)
        setEstaticEuro(euroValue)
        // Show Content
        setIsShowContent(true)
      })
      .catch(error => {
        console.log(error)
      })
  }, [])

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='95%'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        position='absolute'
        top='20px'
      >
        {
          !isShowContent
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={70}
              width={70}
            />
            :
            <React.Fragment>
              <Title
                color='black'
                fontSize='26px'
                margin='50px 0 25px'
              >
                Bienvenido a Transfercoin
              </Title>
              <Title
                color='black'
                fontSize='18px'
                fontWeight='400'
                margin='0 0 10px 0'
              >
                Envía dinero y realiza pagos en el extranjero con menos intereses que cualquier plataforma.
              </Title>
              <Title
                color='black'
                fontSize='18px'
                fontWeight='400'
                margin='0'
              >
                Calcula en los siguientes campos el monto de nuestras monedas:
              </Title>
              <Container
                width='400px'
                margin='50px 0'
              >
                <Container
                  width='100%'
                  height='35px'
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                >
                  <Title
                    fontSize='18px'
                    margin='0'
                  >
                    Petro ₽
                  </Title>
                  <img
                    src={ImgPetro}
                    alt='petro'
                    style={{ height: '100%', marginRight: '7px' }}
                  />
                  <input
                    type='number'
                    style={{ height: '100%', textAlign: 'center', fontWeight: 'bold', width:'210px' }}
                    value={petro}
                    onChange={handleChangePetro}
                  />
                </Container>
                <Container
                  width='100%'
                  height='35px'
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                  margin='20px 0'
                >
                  <Title
                    fontSize='18px'
                    margin='0'
                  >
                    USD $
                  </Title>
                  <img
                    src={ImgDolar}
                    alt='petro'
                    style={{ height: '100%' }}
                  />
                  <input
                    type='number'
                    style={{ height: '100%', textAlign: 'center', fontWeight: 'bold', width:'210px' }}
                    value={dolar}
                    onChange={handleChangeDolar}
                  />
                </Container>
                <Container
                  width='100%'
                  height='35px'
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                >
                  <Title
                    fontSize='18px'
                    margin='0'
                  >
                    Euro €
                  </Title>
                  <img
                    src={ImgEuro}
                    alt='petro'
                    style={{ height: '100%', marginRight: '4px' }}
                  />
                  <input
                    type='number'
                    style={{ height: '100%', textAlign: 'center', fontWeight: 'bold', width:'210px' }}
                    value={euro}
                    onChange={handleChangeEuro}
                  />
                </Container>
              </Container>
              <NavLinkStyled
                to='/transaction'
                width='250px'
                height='35px'
                display='flex'
                alignItems='center'
                justifyContent='center'
                background_color='#2ed06e'
                color='#fff'
                textDecoration='none'
                margin='0 0 51px'
                borderRadius='2px'
                transition='all .15s ease-in-out'
                transitionHover='#28b862'
                fontWeight='500'
              >
                Enviar dinero
              </NavLinkStyled>
            </React.Fragment>
        }
      </Container>
    </Container>
  )
}

export default CalculatorCoins