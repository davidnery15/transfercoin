import { push } from 'react-router-redirect'
import { auth } from '../config/firebase'

export const authenticate = () => {
  auth.onAuthStateChanged((user) => {
    if (user === null) {
      push('/')
    }
  })
}