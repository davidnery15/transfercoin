import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: "AIzaSyAXrFLRj_zrMhrEshknr8TbW3o7BXIxDUc",
  authDomain: "transfercoin-9130b.firebaseapp.com",
  databaseURL: "https://transfercoin-9130b.firebaseio.com",
  projectId: "transfercoin-9130b",
  storageBucket: "gs://transfercoin-9130b.appspot.com",
  messagingSenderId: "584174425042",
  appId: "1:584174425042:web:8d497e27dcd78196082e0b"
};
firebase.initializeApp(firebaseConfig);

export const storage = firebase.storage();
export const auth = firebase.auth()
export const db = firebase.database()
