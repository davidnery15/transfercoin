import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  Button
} from '../styled-components'
import { push } from 'react-router-redirect'
import { auth, db } from '../../config/firebase'
import Loader from 'react-loader-spinner'
import Modal from 'react-modal'
import { toast } from 'react-toastify'

import './modal.css'
import IconUserCard from '../../assets/icon-user-card.png'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}
Modal.setAppElement('#modal')

function CardsFetch() {
  const [contentShow, setContentShow] = useState(false)
  const [userCards, setUserCards] = useState([])
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [uid, setUid] = useState('')

  const handleModalToggle = e => {
    e.preventDefault()
    setModalIsOpen(!modalIsOpen)
  }

  const handleAddCard = e => {
    e.preventDefault()
    if (userCards.length > 3) {
      toast.error('Solo puede registrar maximo 4 tarjetas')
    } else {
      push('/cards/add-card')
    }
  }

  const handleDeleteCard = (e, idCard) => {
    e.preventDefault()
    db.ref(`cards/${uid}/${idCard}`).remove()
      .then(() => {
        window.location.reload()
      })
  }

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user === null) {
        push('/')
      } else {
        const uid = user.uid
        db.ref(`cards/${uid}`).once("value", (snapshot) => {
          if(snapshot.val() !== null) {
            const cards = Object.values(snapshot.val())
          setUserCards(cards)
          }
          setUid(uid)
          setContentShow(true)
        })
      }
    })
  }, [])

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='95%'
        min-height='60%'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        justifyContent='center'
        position='absolute'
        top='20px'
        padding='40px 0 40px'
      >
        {
          !contentShow
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={40}
              width={40}
            />
            :
            userCards.length > 0
              ?
              userCards.map((card, i) => {
                const idCard = card.id
                return (
                  <Container
                    key={i}
                    width='250px'
                    height='50px'
                    display='flex'
                    alignItems='center'
                    justifyContent='space-between'
                    padding='0 30px'
                    background='transparent'
                    margin='0 0 30px'
                    border='2px solid #000'
                    borderRadius='5px'
                    position='relative'
                  >
                    <img
                      src={IconUserCard}
                      alt='icon-user-card'
                      style={{ width: '30px', height: '30px' }}
                    />
                    <Title
                      color='black'
                      fontSize='18px'
                    >
                      {card.cardNumber}
                    </Title>
                    <Button
                      width='25px'
                      height='25px'
                      color='red'
                      fontSize='20px'
                      fontWeight='bold'
                      background='white'
                      border='none'
                      borderRadius='100px'
                      outline='none'
                      position='absolute'
                      top='-13px'
                      right='-13px'
                      onClick={handleModalToggle}
                    >
                      x
                    </Button>
                    <Modal
                      isOpen={modalIsOpen}
                      onRequestClose={handleModalToggle}
                      style={customStyles}
                      className="Modal"
                      overlayClassName="Overlay"
                    >
                      <Container
                        background_color='white'
                        padding='40px'
                        borderRadius='10px'
                      >
                        <Title
                          color='black'
                          fontSize='18px'
                        >
                          ¿ Seguro que desea eliminar esta tarjeta ?
                        </Title>
                        <Container
                          width='100%'
                          height='40px'
                          display='flex'
                          justifyContent='space-around'
                          margin='40px 0 0'
                        >
                          <Button
                            width='100px'
                            background='transparent'
                            border='2px solid #000'
                            fontWeight='bold'
                            outline='none'
                            onClick={handleModalToggle}
                          >
                            Cancelar
                          </Button>
                          <Button
                            width='100px'
                            background='transparent'
                            border='2px solid red'
                            color='red'
                            fontWeight='bold'
                            outline='none'
                            onClick={(e) => (handleDeleteCard(e, idCard))}
                          >
                            Eliminar
                          </Button>
                        </Container>
                      </Container>
                    </Modal>
                  </Container>
                )
              })
              :
              <Title
                color='black'
                fontSize='26px'
              >
                No hay tarjetas registradas
            </Title>
        }
        <Button
          width='314px'
          height='45px'
          border='none'
          background_color='#36558e'
          color='white'
          fontWeight='400'
          outline='none'
          textDecoration='none'
          display='flex'
          alignItems='center'
          justifyContent='center'
          borderRadius='5px'
          onClick={handleAddCard}
        >
          Agregar tarjeta
        </Button>
      </Container>
    </Container>
  )
}

export default CardsFetch