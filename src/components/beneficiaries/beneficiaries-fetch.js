import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  Button
} from '../styled-components'
import { push } from 'react-router-redirect'
import { auth, db } from '../../config/firebase'
import Loader from 'react-loader-spinner'
import Modal from 'react-modal'

import './modal.css'
import IconUserBeneficiarie from '../../assets/icon-user-beneficiarie.jpg'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}
Modal.setAppElement('#modal')

function BeneficiariesFetch() {
  const [contentShow, setContentShow] = useState(false)
  const [userBeneficiaries, setUserBeneficiaries] = useState([])
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [uid, setUid] = useState('')

  const handleModalToggle = e => {
    e.preventDefault()
    setModalIsOpen(!modalIsOpen)
  }

  const handleAddBeneficiarie = e => {
    e.preventDefault()
    push('/beneficiaries/add-beneficiarie')
  }

  const handleDeleteBeneficiarie = (e, idBeneficiarie) => {
    e.preventDefault()
    db.ref(`beneficiaries/${uid}/${idBeneficiarie}`).remove()
      .then(() => {
        window.location.reload()
      })
  }

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user === null) {
        push('/')
      } else {
        const uid = user.uid
        db.ref(`beneficiaries/${uid}`).once("value", (snapshot) => {
          if (snapshot.val() !== null) {
            const beneficiariesArray = Object.values(snapshot.val())
            setUserBeneficiaries(beneficiariesArray)
          }
          setUid(uid)
          setContentShow(true)
        })
      }
    })
  }, [])

  return (
    <Container
      background_color='#f2f2f2'
      height='90vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
    >
      <Container
        width='95%'
        min-height='60%'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        justifyContent='center'
        position='absolute'
        top='20px'
        padding='40px 0 40px'
      >
        {
          !contentShow
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={40}
              width={40}
            />
            :
            userBeneficiaries.length > 0
              ?
              userBeneficiaries.map((person, i) => {
                const idBeneficiarie = person.id
                return (
                  <Container
                    key={i}
                    width='250px'
                    height='50px'
                    display='flex'
                    alignItems='center'
                    padding='0 30px'
                    background='transparent'
                    margin='0 0 30px'
                    border='2px solid #000'
                    borderRadius='5px'
                    position='relative'
                  >
                    <img
                      src={IconUserBeneficiarie}
                      alt='icon-user-beneficiarie'
                      style={{ width: '38px', height: '38px', marginRight: '15px' }}
                    />
                    <Title
                      color='black'
                      fontSize='18px'
                    >
                      {person.name}
                    </Title>
                    <Button
                      width='25px'
                      height='25px'
                      color='red'
                      fontSize='20px'
                      fontWeight='bold'
                      background='white'
                      border='none'
                      borderRadius='100px'
                      outline='none'
                      position='absolute'
                      top='-13px'
                      right='-13px'
                      onClick={handleModalToggle}
                    >
                      x
                    </Button>
                    <Modal
                      isOpen={modalIsOpen}
                      onRequestClose={handleModalToggle}
                      style={customStyles}
                      className="Modal"
                      overlayClassName="Overlay"
                    >
                      <Container
                        background_color='white'
                        padding='40px'
                        borderRadius='10px'
                      >
                        <Title
                          color='black'
                          fontSize='18px'
                        >
                          ¿ Seguro que desea eliminar este destinatario ?
                        </Title>
                        <Container
                          width='100%'
                          height='40px'
                          display='flex'
                          justifyContent='space-around'
                          margin='40px 0 0'
                        >
                          <Button
                            width='100px'
                            background='transparent'
                            border='2px solid #000'
                            fontWeight='bold'
                            outline='none'
                            onClick={handleModalToggle}
                          >
                            Cancelar
                          </Button>
                          <Button
                            width='100px'
                            background='transparent'
                            border='2px solid red'
                            color='red'
                            fontWeight='bold'
                            outline='none'
                            onClick={(e) => (handleDeleteBeneficiarie(e, idBeneficiarie))}
                          >
                            Eliminar
                          </Button>
                        </Container>
                      </Container>
                    </Modal>
                  </Container>
                )
              })
              :
              <Title
                color='black'
                fontSize='26px'
              >
                No hay destinatarios agregados
            </Title>
        }
        <Button
          width='314px'
          height='45px'
          border='none'
          background_color='#36558e'
          color='white'
          fontWeight='400'
          outline='none'
          textDecoration='none'
          display='flex'
          alignItems='center'
          justifyContent='center'
          borderRadius='5px'
          onClick={handleAddBeneficiarie}
        >
          Agregar destinatario
        </Button>
      </Container>
    </Container>
  )
}

export default BeneficiariesFetch