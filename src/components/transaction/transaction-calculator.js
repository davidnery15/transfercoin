import React, { useState, useEffect } from 'react'
import {
  Container,
  Title,
  Form,
  Select,
  Input,
  NumberFormatInput,
  Button
} from '../styled-components'
import Loader from 'react-loader-spinner'
import { auth, db } from '../../config/firebase'
import { push } from 'react-router-redirect'
import { toast } from 'react-toastify'

function CalculatorCoins() {
  // Transaction
  const [sendCoin, setSendCoin] = useState('Dolar (USD)')
  const [sendMoney, setSendMoney] = useState(1)
  const [receiveCoin, setReceiveCoin] = useState('Euro €')
  const [receiveMoney, setReceiveMoney] = useState(0.86)
  const [commission, setCommission] = useState(0.02)
  // Coins
  const [estaticDolar, setEstaticDolar] = useState(0)
  // Beneficiarie
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [iban, setIban] = useState('')
  // card
  const [cardName, setCardName] = useState('')
  const [cardNumber, setCardNumber] = useState('')
  const [cardDate, setCardDate] = useState('')
  const [cardCode, setCardCode] = useState('')
  // Set Firebase arrays
  const [userBeneficiaries, setUserBeneficiaries] = useState([])
  const [userCards, setUserCards] = useState([])
  // get beneficiarie and card
  const [dataBeneficiare, setDataBeneficiare] = useState('')
  const [dataCard, setDataCard] = useState('')
  // others
  const [uid, setUid] = useState('')
  const [isShowContent, setIsShowContent] = useState(false)

  const coinCalculator = (currentCoin, receiveCoin, actualSendMoney, commission) => {
    if (currentCoin === 'Dolar (USD)' && receiveCoin === 'Euro €') {
      // set Euro
      const value = (actualSendMoney - (actualSendMoney * 0.12) - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Dolar (USD)' && receiveCoin === 'Dolar (USD)') {
      // set Dolar
      const value = (actualSendMoney - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Dolar (USD)' && receiveCoin === 'Petro ₽') {
      // set Petro
      const value = (actualSendMoney / estaticDolar).toFixed(8)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Euro €' && receiveCoin === 'Euro €') {
      // set Euro
      const value = (actualSendMoney - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Euro €' && receiveCoin === 'Dolar (USD)') {
      // set Dolar
      const value = (actualSendMoney * 1.13 - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Euro €' && receiveCoin === 'Petro ₽') {
      // set Dolar
      const estaticDolarForEuro = (estaticDolar - (estaticDolar * 0.12)).toFixed(2)
      const value = (actualSendMoney / estaticDolarForEuro).toFixed(8)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Petro ₽' && receiveCoin === 'Euro €') {
      // set Petro
      const estaticEuro = (estaticDolar - (estaticDolar * 0.12)).toFixed(2)
      const value = ((actualSendMoney * estaticEuro) - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Petro ₽' && receiveCoin === 'Dolar (USD)') {
      // set Petro
      const value = ((actualSendMoney * estaticDolar) - commission).toFixed(2)
      setReceiveMoney(value)
    }
    if (currentCoin === 'Petro ₽' && receiveCoin === 'Petro ₽') {
      // set Petro
      const value = (actualSendMoney - 0.0000008).toFixed(8)
      setReceiveMoney(value)
    }
  }

  const handleChangeSendCoin = e => {
    setSendCoin(e.target.value)
    // set commission
    const commission = (sendMoney * 0.02).toFixed(2)
    setCommission(commission)
    coinCalculator(e.target.value, receiveCoin, sendMoney, commission)
  }

  const handleChangeSendMoney = e => {
    setSendMoney(e.target.value)
    // set commission
    const commission = (e.target.value * 0.02).toFixed(2)
    setCommission(commission)
    coinCalculator(sendCoin, receiveCoin, e.target.value, commission)
  }

  const handleChangeReceiveCoin = e => {
    setReceiveCoin(e.target.value)
    // set commission
    const commission = (sendMoney * 0.02).toFixed(2)
    setCommission(commission)
    coinCalculator(sendCoin, e.target.value, sendMoney, commission)
  }

  //Change Beneficiarie
  const handleChangeDataBeneficiare = e => {
    setDataBeneficiare(e.target.value)
    if (e.target.value === '') {
      setName('')
      setEmail('')
      setIban('')
    } else {
      const i = e.target.value
      const currentBeneficiarie = userBeneficiaries[i]
      setName(currentBeneficiarie.name)
      setEmail(currentBeneficiarie.email)
      setIban(currentBeneficiarie.iban)
    }
  }
  const handleChangeName = e => setName(e.target.value)
  const handleChangeEmail = e => setEmail(e.target.value)
  const handleChangeIban = e => setIban(e.target.value)
  //Change Card
  const handleChangeDataCard = e => {
    setDataCard(e.target.value)
    if (e.target.value === '') {
      setCardName('')
      setCardNumber('')
      setCardDate('')
      setCardCode('')
    } else {
      const i = e.target.value
      const currentCard = userCards[i]
      setCardName(currentCard.cardName)
      setCardNumber(currentCard.cardNumber)
      setCardDate(currentCard.cardDate)
      setCardCode(currentCard.cardCode)
    }
  }
  const handleChangeCardName = e => setCardName(e.target.value)
  const handleChangeCardNumber = e => setCardNumber(e.target.value)
  const handleChangeCardDate = e => setCardDate(e.target.value)
  const handleChangeCardCode = e => setCardCode(e.target.value)

  useEffect(() => {
    fetch('https://s3.amazonaws.com/dolartoday/data.json')
      .then(response => {
        return response.json()
      })
      .then(object => {
        // Set static Dolar about Petro
        const stringDolar = object.MISC.petroleo
        const formatedStringDolar = stringDolar.replace(',', '.')
        const dolarValue = parseFloat(formatedStringDolar)
        setEstaticDolar(dolarValue)
        setIsShowContent(true)
      })
      .catch(error => {
        console.log(error)
      })
  }, [])

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user === null) {
        push('/')
      } else {
        const uid = user.uid
        setUid(uid)
        db.ref(`beneficiaries/${uid}`).once("value", (snapshot) => {
          if (snapshot.val() !== null) {
            const beneficiariesArray = Object.values(snapshot.val())
            setUserBeneficiaries(beneficiariesArray)
          }
        })
        db.ref(`cards/${uid}`).once("value", (snapshot) => {
          if (snapshot.val() !== null) {
            const cardsArray = Object.values(snapshot.val())
            setUserCards(cardsArray)
          }
        })
      }
    })
  }, [])

  const handleSubmit = e => {
    e.preventDefault()
    if (
      sendMoney === '' ||
      name === '' ||
      email === '' ||
      iban === '' ||
      cardName === '' ||
      cardNumber === '' ||
      cardDate === '' ||
      cardCode === ''
    ) {
      toast.error('Completa los campos vacíos')
    } else {
      const regExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,4})+$/
      const isValidateEmail = regExp.test(email)

      if (!isValidateEmail) {
        toast.error('Coloca un correo válido')
      } else {
        const data = db.ref(`transactions/${uid}`).push()
        data.set({
          sendCoin,
          sendMoney,
          receiveCoin,
          receiveMoney,
          beneficiarieName: name,
          beneficiarieEmail: email,
          beneficiarieIban: iban,
          cardName,
          cardNumber,
          cardDate,
          cardCode,
          uid,
          approved: 'waiting',
          id: data.key
        })
        push('/transaction-confirm')
      }
    }
  }

  return (
    <Container
      background_color='#f2f2f2'
      height='auto'
      minHeight='82vh'
      boxShadow='inset 0 0 15px 0 rgba(0,0,0,.25)'
      display='flex'
      justifyContent='center'
      position='relative'
      padding='30px 20px'
    >
      <Container
        width='95%'
        background_color='#fff'
        boxShadow='0 13px 26px 0 rgba(0,0,0,.25)'
        display='flex'
        flexDirection='column'
        alignItems='center'
        padding='0 0 40px'
      >
        {
          !isShowContent
            ?
            <Loader
              type="ThreeDots"
              color="#000"
              height={70}
              width={70}
            />
            :
            <React.Fragment>
              <Title
                color='black'
                fontSize='22px'
                margin='50px 0 25px'
              >
                Ingrese la siguiente información para la gestión de la transacción:
              </Title>
              <Form
                width='400px'
                margin='50px 0'
                onSubmit={handleSubmit}
              >
                <Title
                  fontSize='16px'
                  margin='10px 0'
                >
                  Envías:
                </Title>
                <Container
                  width='100%'
                  height='39px'
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                >
                  <Select
                    width='40%'
                    height='100%'
                    value={sendCoin}
                    onChange={handleChangeSendCoin}
                  >
                    <option value='Dolar (USD)'>Dolar (USD)</option>
                    <option value='Euro €'>Euro €</option>
                    <option value='Petro ₽'>Petro ₽</option>
                  </Select>
                  <Input
                    width='60%'
                    height='35px'
                    padding='0 20px'
                    type='number'
                    value={sendMoney}
                    onChange={handleChangeSendMoney}
                  />
                </Container>
                <Title
                  fontSize='16px'
                  margin='20px 0 10px'
                >
                  Destinatario obtiene:
                </Title>
                <Container
                  width='100%'
                  height='39px'
                  display='flex'
                  alignItems='center'
                  justifyContent='space-between'
                >
                  <Select
                    width='40%'
                    height='100%'
                    value={receiveCoin}
                    onChange={handleChangeReceiveCoin}
                  >
                    <option value='Euro €'>Euro €</option>
                    <option value='Dolar (USD)'>Dolar (USD)</option>
                    <option value='Petro ₽'>Petro ₽</option>
                  </Select>
                  <Input
                    width='60%'
                    height='35px'
                    padding='0 20px'
                    type='number'
                    disabled={true}
                    value={receiveMoney}
                  />
                </Container>
                <Title
                  fontSize='16px'
                  margin='20px 0 10px'
                  fontWeight='300'
                >
                  Comision total: {commission} {sendCoin}
                </Title>
                <Title
                  fontSize='20px'
                  margin='40px 0'
                  textAlign='center'
                >
                  Ingrese los datos del destinatario
                </Title>
                {
                  userBeneficiaries.length > 0
                    ?
                    <Container>
                      <Title
                        color='black'
                        fontSize='14px'
                      >
                        Seleccione un beneficiario agregado
                      </Title>
                      <Select
                        width='100%'
                        height='35px'
                        value={dataBeneficiare}
                        onChange={handleChangeDataBeneficiare}
                      >
                        <option value=''>Ninguno</option>
                        {
                          userBeneficiaries.map((beneficiare, i) => {
                            return (
                              <option
                                key={i}
                                value={i}
                              >
                                {beneficiare.name}
                              </option>
                            )
                          })
                        }
                      </Select>
                    </Container>
                    : null
                }
                <Title
                  color='black'
                  fontSize='14px'
                >
                  Nombre completo
                </Title>
                <Input
                  type='text'
                  width='94%'
                  height='30px'
                  padding='0 10px'
                  value={name}
                  onChange={handleChangeName}
                  disabled={dataBeneficiare === '' ? false : true}
                />
                <Title
                  color='black'
                  fontSize='14px'
                >
                  Correo
                </Title>
                <Input
                  type='text'
                  width='94%'
                  height='30px'
                  padding='0 10px'
                  value={email}
                  onChange={handleChangeEmail}
                  disabled={dataBeneficiare === '' ? false : true}
                />
                <Title
                  color='black'
                  fontSize='14px'
                >
                  N° de IBAN
                </Title>
                <Input
                  type='text'
                  width='94%'
                  height='30px'
                  padding='0 10px'
                  value={iban}
                  onChange={handleChangeIban}
                  disabled={dataBeneficiare === '' ? false : true}
                />
                <Title
                  fontSize='20px'
                  margin='40px 0'
                  textAlign='center'
                >
                  Ingrese los datos de la tarjeta
                </Title>
                {
                  userCards.length > 0
                    ?
                    <Container>
                      <Title
                        color='black'
                        fontSize='14px'
                      >
                        Seleccione una tarjeta registrada
                      </Title>
                      <Select
                        width='100%'
                        height='35px'
                        value={dataCard}
                        onChange={handleChangeDataCard}
                      >
                        <option value=''>Ninguna</option>
                        {
                          userCards.map((card, i) => {
                            return (
                              <option
                                key={i}
                                value={i}
                              >
                                {card.cardNumber}
                              </option>
                            )
                          })
                        }
                      </Select>
                    </Container>
                    : null
                }
                <Title
                  color='black'
                  fontSize='14px'
                >
                  Nombre del titular
                </Title>
                <Input
                  type='text'
                  width='94%'
                  height='30px'
                  padding='0 10px'
                  value={cardName}
                  onChange={handleChangeCardName}
                  disabled={dataCard === '' ? false : true}
                />
                <Title
                  color='black'
                  fontSize='14px'
                >
                  Número de tarjeta
                </Title>
                <NumberFormatInput
                  width='94%'
                  height='30px'
                  padding='0 10px'
                  format="####  ####  ####  ####"
                  value={cardNumber}
                  onChange={handleChangeCardNumber}
                  disabled={dataCard === '' ? false : true}
                />
                <Container
                  width='96%'
                  display='flex'
                  justifyContent='space-between'
                >
                  <Container
                    width='45%'
                  >
                    <Title
                      color='black'
                      fontSize='14px'
                    >
                      Fecha de vencimiento
                    </Title>
                    <NumberFormatInput
                      width='94%'
                      height='30px'
                      padding='0 10px'
                      format="## / ##"
                      placeholder="mm / aa"
                      value={cardDate}
                      onChange={handleChangeCardDate}
                      disabled={dataCard === '' ? false : true}
                    />
                  </Container>
                  <Container
                    width='45%'
                  >
                    <Title
                      color='black'
                      fontSize='14px'
                    >
                      Código de seguridad
                    </Title>
                    <input
                      type='password'
                      maxLength="3"
                      style={{ width: '94%', height: '30px', padding: '0 10px' }}
                      value={cardCode}
                      onChange={handleChangeCardCode}
                      disabled={dataCard === '' ? false : true}
                    />
                  </Container>
                </Container>
                <Container
                  width='100%'
                  display='flex'
                  justifyContent='center'
                  margin='30px 0 0'
                >
                  <Button
                    width='100%'
                    height='35px'
                    border='none'
                    background_color='#36558e'
                    color='white'
                    fontWeight='400'
                    outline='none'
                    type='submit'
                  >
                    Enviar Transferencia
                  </Button>
                </Container>
              </Form>
            </React.Fragment>
        }
      </Container>
    </Container>
  )
}

export default CalculatorCoins